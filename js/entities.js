//namespace definition
var JH = JH || {};

JH.CONSTANTS = {
    BEAT_MILLIS: 500,
    SONG_LENGTH_SECONDS: 62,
    SONG_LENGTH_MILLIS: 62000,

    FRAME_X: 450,
    FRAME_Y: 220,
    HAMMER_X: 450,
    HAMMER_Y_TOP: 100,
    HAMMER_Y_BOTTOM: 320,
    ARROW_X: 482,
    ARROW_Y_START: 460,
    ARROW_Y_MARGIN: 70,
    NEXT_ARROW_X: 620,
    NEXT_ARROW_Y_START: 460,
    NEXT_ARROW_Y_MARGIN: 40,

    TRAIN_PARTICLE_COOLDOWN: 1000,
    PARTICLE_LAYER: 5,

    STATE_COUNTDOWN: 1,
    STATE_PLAYING: 2,
    STATE_LEVEL_END: 3
};



JH.actionSortFunction = function (a, b)
{
    if (a.x !== b.x)
    {
        //normally sort by x value
        return a.x - b.x;
    } else
    {
        return a.sequenceIndex - b.sequenceIndex;
    }
};

///////////////////////////////////////////////////////////
//  PLAYER ENTITY
///////////////////////////////////////////////////////////
JH.PlayerEntity = me.ObjectEntity.extend(
{

	init:function (x, y, settings)
	{
	    var i, track, xpos;

	    //timing
	    JH.lastTime = new Date().getTime();
	    JH.currTime = null;
	    JH.millis= 0;
		
		// call the constructor
		settings.image = "jh";
		settings.spritewidth  = 185;
		settings.spriteheight = 131;
		y -= 48;
		this.parent(x, y , settings);
		//this.renderable.resize(2);
	    // set the walking & jumping speed
        //set to large number since it will be updated by tweener... might not need it at all
		this.setVelocity(100,100);
				
		// adjust the bounding box
//		this.updateColRect(8, 16, 0, 32);
		
		// set the display to follow our position on both axis
		this.jumping = false;
		this.falling = false;
		this.gravity = 0;
		this.renderable.alwaysUpdate = true;

		me.game.viewport.pos.x = 0;
		this.pos.x = x;
		this.pos.y = y;
		this.source = new me.Vector2d(x, y);
		this.destination = new me.Vector2d(x, y);
		this.t = 0;
		this.tweener = new me.Tween(this);
		this.tweener.to({ t: 1 });
        
		JH.beatKeeper = null;
		JH.beatKeeper = new JH.BeatKeeper(this, []);
		JH.beatKeeper.init(x, y, settings);

	    //add this object to the global namespace so others can reference it
		JH.player = null;
		JH.player = this;

	    //create a train object
		this.train = me.entityPool.newInstanceOf('train', x, y, settings);
		this.train.pos.y -= .65 * this.train.collisionBox.height;

	    //namespace globals
		JH.actionList = [];
		JH.actionIndex = 0;
		JH.sequenceIndex = 0;
		JH.sequenceStart = 0;
		JH.actionsSorted = false;
		JH.state = JH.CONSTANTS.STATE_COUNTDOWN;
	    //timekeeping
		JH.lastTime = 0;
		JH.currTime = 0;
		JH.millis = 0; //millis since last update

	    //create tracks for player and train
		for (xpos = x; xpos < me.game.currentLevel.cols * me.game.currentLevel.tilewidth - 64; xpos += 32)
		{
		    track = me.entityPool.newInstanceOf('track', xpos, y+124, { followPlayer: true });
		    me.game.add(track, JH.CONSTANTS.PARTICLE_LAYER);
		    track = me.entityPool.newInstanceOf('track', xpos, y- this.collisionBox.height+154, { followPlayer: false });
		    me.game.add(track, JH.CONSTANTS.PARTICLE_LAYER);
		}
		me.game.sort();

		//JH.beatKeeper.beatID = setInterval((function () { JH.beatKeeper.update(); }),
        //    JH.CONSTANTS.BEAT_MILLIS);

		this.renderable.addAnimation("stand", [0, 0, 0, 1,1,1]);
		this.renderable.addAnimation("walk", [11, 12]);//[10, 10, 11, 12, 12,11]);
		this.renderable.addAnimation("hammer", [5,6,7,8,9,9]);
		this.renderable.setCurrentAnimation("hammer", "hammer");
	
	},
	
	setMovement: function(srcx, srcy, destx, desty) {
	    this.source.set(srcx, srcy);
	    this.destination.set(destx, desty);
	    this.t = 0;
	    this.tweener.stop(); //dummny check
	    this.tweener.to({ t: 1 }, JH.CONSTANTS.BEAT_MILLIS);
	    this.tweener.easing(me.Tween.Easing.Quadratic.InOut);
	    this.tweener.start();
	},

	update : function ()
	{
	    var input, action;

	    //timing
	    JH.currTime = new Date().getTime();
	    JH.millis= JH.currTime - JH.lastTime;
	    if (JH.millis> 500) JH.millis= 16;

	    //update the JH sequence stuff
	    if (!JH.actionsSorted)
	    {
	        if (JH.level > 1) {
                if (me.game.viewport._fadeIn.tween) me.game.viewport._fadeIn.tween.stop();
                if (me.game.viewport._fadeIn.alpha) me.game.viewport._fadeIn.alpha = 0;
	            me.game.viewport.fadeOut('#ffffff', 500);
	        }
            //beginning of game
	        JH.actionList.sort( JH.actionSortFunction );
	        JH.actionsSorted = true;
	    }

		//input
	    if (me.input.isKeyPressed('top')) {
	        input = 'a';
	    } else if (me.input.isKeyPressed('mid') || me.input.isKeyPressed('mid2')) {
	        input = 'c';
	    } else if (me.input.isKeyPressed('bot'))
	    {
	        input = 'b';
	    }

	    //test input
	    if (JH.state === JH.CONSTANTS.STATE_PLAYING) {
	        action = JH.actionList[JH.actionIndex];

	        //spawn some dust particles for sequences
	        //spawn particles
	        if (action && action.sequenceIndex !== undefined) {
	            JH.spawnParticles(2, { x: 1, y: -1 }, 2 * Math.PI,
                    this.destination.x + .8 * this.collisionBox.width, this.destination.y + .5 * this.collisionBox.height,
                    { layer: 10, r: 153, g: 135, b: 115, gravity: -.02, float: false, duration: JH.CONSTANTS.TRAIN_PARTICLE_COOLDOWN * .7, vel: .8 });
	        }

	        if (action && input === action.stepList[action.stepIndex]) {
	            //good input, move to the next step
	            if (action.stepIndex < action.stepList.length) {
	                action.stepIndex += 1;
	            }
	            //TODO sound cue?
	        }
	    }

	    //set animation
	    if (this.t !== 0 && this.t !== 1 && this.source.x !== this.destination.x) {
	        //set to moving
	        if (!this.renderable.isCurrentAnimation('walk')) {
	            this.renderable.setCurrentAnimation("walk", "walk");
	            this.renderable.setAnimationFrame();
	        }
	    } else {
	        //set to standing
	        if (JH.state === JH.CONSTANTS.STATE_PLAYING) {
	            if (!this.renderable.isCurrentAnimation('hammer')) {
	                this.renderable.setCurrentAnimation("hammer", "hammer");
	                this.renderable.setAnimationFrame();
	            }
	        } else {
	            if (!this.renderable.isCurrentAnimation('stand')) {
	                this.renderable.setCurrentAnimation("stand", "stand");
	                this.renderable.setAnimationFrame();
	            }
	        }
	    }
		
	    this.pos.x = ((1 - this.t) * this.source.x) + (this.t * this.destination.x);
	    this.pos.x = Math.floor(this.pos.x);
	    this.pos.y = ((1 - this.t) * this.source.y) + (this.t * this.destination.y);

	    //move the camera to follow the action
	    if (this.pos.x > me.game.viewport.pos.x + JH.CONSTANTS.FRAME_X - 2 * this.renderable.width)
	    {
	        me.game.viewport.pos.x = this.pos.x - (JH.CONSTANTS.FRAME_X - 2 * this.renderable.width);
            
	        //make sure you don't go too far
	        if (me.game.viewport.pos.x + me.game.viewport.width > (me.game.currentLevel.cols * me.game.currentLevel.tilewidth))
	        {
	            me.game.viewport.pos.x = (me.game.currentLevel.cols * me.game.currentLevel.tilewidth) - me.game.viewport.width;
	        }
	    } else if (this.pos.x < me.game.viewport.x + me.game.viewport.width)
	    {
	        //get the player back in view
	        me.game.viewport.pos.x = this.pos.x - JH.CONSTANTS.FRAME_X - 2 * this.renderable.width;
	        if (me.game.viewport.pos.x < 0) me.game.viewport.pos.x = 0;
	    }

	    this.parent(this);

	    //update train
	    JH.beatKeeper.update();
	    this.train.update();

		JH.lastTime = JH.currTime;
		return true;
	
	},

	draw: function (context)
	{
	    this.train.draw(context);
	    this.parent(context);
	    JH.beatKeeper.draw(context);
	}
});

///////////////////////////////////////////////////////////
//  BEAT KEEPER
///////////////////////////////////////////////////////////
JH.BeatKeeper = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        //load images
        this.threeImage = me.loader.getImage("3");
        this.twoImage = me.loader.getImage("2");
        this.oneImage = me.loader.getImage("1");
        this.driveImage = me.loader.getImage("drive");
        this.hammerImage = me.loader.getImage("hammer");
        this.frameImage = me.loader.getImage("frame");

        this.arrows = {};
        this.arrows['upImage'] = me.loader.getImage("up");
        this.arrows['upCompleteImage'] = me.loader.getImage("up_complete");
        this.arrows['downImage'] = me.loader.getImage("down");
        this.arrows['downCompleteImage'] = me.loader.getImage("down_complete");
        this.arrows['rightImage'] = me.loader.getImage("right");
        this.arrows['rightCompleteImage'] = me.loader.getImage("right_complete");
        
        //init variables
        this.beatProgress = 1;
        this.beat = 0;
        this.measure = 0;
        this.countdown = 3;

        this.audioID = null;

        this.lastBeat = new Date().getMilliseconds();
        this.time = 0;
    },

    update: function()
    {
        this.time += JH.millis;
        if (this.beat % 2 === 0) {
            //upswing
            this.beatProgress = 1 - this.easeInOutCubic(this.time, 0, 1, JH.CONSTANTS.BEAT_MILLIS);
        } else {
            //downswing
            this.beatProgress = this.easeInExpo(this.time, 0, 1, JH.CONSTANTS.BEAT_MILLIS);
        }
        if (this.beatProgress > 1) this.beatProgress = 1;
        if (this.beatProgress < 0) this.beatProgress = 0;

        if (this.time >= JH.CONSTANTS.BEAT_MILLIS)
        {
            this.handleBeat();
            this.time -= JH.CONSTANTS.BEAT_MILLIS;
        }
    },

    //easing in out based on easing equations by Robert Penner, George McGinley Smith
    easeInOutCubic: function (t, b, c, d) {
        if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;
        return c / 2 * ((t -= 2) * t * t + 2) + b;
    },
    easeInExpo: function (t, b, c, d) {
        return (t == 0) ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
    },
    handleBeat: function ()
    {
        var action, i, srcx, srcy, destx, desty;
        //this.tweener.stop(); //dummy check
        //this.tweenBack.stop();
        if (JH.state === JH.CONSTANTS.STATE_PLAYING)
        {
            this.beat += 1;
            if (this.beat >= 4)
            {
                this.beat -= 4;
                this.measure += 1;
                //me.audio.stop("drum"); //fix any weird timing mishaps
                //me.audio.play("drum"); //keep the drums going
            }
            if (this.beat % 2 === 0)
            {
                //finished the downswing
                //play audio
                //me.audio.stop("hit");
                //me.audio.play("hit", false, null, .6);

                //spawn particles
                JH.spawnParticles(20, { x: 1, y: -.3 }, .5 * Math.PI,
                    me.game.viewport.pos.x + JH.CONSTANTS.HAMMER_X + 5 + (.5 * this.hammerImage.width), JH.CONSTANTS.FRAME_Y + this.frameImage.height,
                    { layer: JH.CONSTANTS.PARTICLE_LAYER, vel: 6.4 });
                JH.spawnParticles(20, { x:-1, y:-.3 }, .5 * Math.PI,
                    me.game.viewport.pos.x + JH.CONSTANTS.HAMMER_X - 5 + (.5 * this.hammerImage.width), JH.CONSTANTS.FRAME_Y + this.frameImage.height,
                    { layer: JH.CONSTANTS.PARTICLE_LAYER, vel: 3.2 });

                //check if the action was completed
                action = JH.actionList[JH.actionIndex];
                if (action.stepIndex === action.stepList.length)
                {
                    //completed, see if that ends a sequence
                    if (action.sequenceIndex !== undefined && action.isSequenceEnd)
                    {
                        //sequence completed
                        //TODO sound?
                    }
                    me.game.viewport.shake(10, 500, me.game.viewport.AXIS.BOTH);
                    me.audio.play("hit");

                    //move the player
                    srcx = action.x;
                    srcy = action.y;

                    // move to next action.
                    JH.actionIndex += 1;


                    //see if you've finished the last action
                    if (JH.actionIndex >= JH.actionList.length) {
                        //we're done, stop working
                        me.audio.stopTrack();

                        if (JH.level < JH.GLOBALS.MAX_LEVEL && JH.state === JH.CONSTANTS.STATE_PLAYING) {
                            me.game.viewport.fadeIn('#ffffff', 1500);
                            JH.state = JH.CONSTANTS.STATE_LEVEL_END;
                            JH.countdown = 3;
                            me.audio.play("success");
                            return true;
                        } else {
                            me.game.viewport.fadeIn('#ffffff', 1500);
                            JH.state = JH.CONSTANTS.STATE_LEVEL_END;
                            JH.countdown = 3;
                            me.audio.play("success");
                            return true;
                            ////GAME OVER - WIN
                            //me.state.change(me.state.GAME_END);
                            //return false;
                        }
                    }

                    action = JH.actionList[JH.actionIndex];

                    //move the player part 2
                    destx = action.x;
                    desty = action.y;
                    JH.player.setMovement(srcx, srcy, destx, desty);

                    //update sequence variables
                    if (action && action.isSequenceStart)
                    {
                        JH.sequenceStart = JH.actionIndex;
                    }

                } else
                {
                    //action was not completed, re-start the action
                    action.stepIndex = 0;
                    me.audio.play("miss");

                    //if it was a sequence, go to start of sequence
                    if (action.sequenceIndex !== undefined)
                    {
                        //reset every previous action
                        for (i = JH.actionIndex; i >= JH.sequenceStart; i--)
                        {
                            action = JH.actionList[i];
                            action.stepIndex = 0;
                        }
                        //go back to the start of the sequence
                        JH.actionIndex = JH.sequenceStart;
                    }

                }
            }
        } else if (JH.state === JH.CONSTANTS.STATE_COUNTDOWN) {
            this.countdown -= 1;
            if (this.countdown < 0) {
                JH.state = JH.CONSTANTS.STATE_PLAYING;
                JH.player.setMovement(JH.player.pos.x, JH.player.pos.y, JH.actionList[0].x, JH.actionList[0].y);
                JH.player.train.start();
                me.audio.playTrack("music");
                //this.audioID = window.setTimeout(function () { me.audio.play("hit"); }, 1000);
            }
        } else // state === level_end
        {
            this.countdown -= 1;
            if (this.countdown < 0) {
                //go to next level
                if (JH.level < JH.GLOBALS.MAX_LEVEL) {
                    JH.level += 1;
                    me.levelDirector.loadLevel("level_" + JH.level);
                    //me.game.viewport.fadeOut("#ffffff", 250);
                } else {
                    //end the game - win
                    me.state.change(me.state.GAME_END);
                }
                return false;
            }
        }
        //this.tweener.start();
        this.lastBeat = JH.currTime;
        return true;
    },

    draw: function (context)
    {
        var i, image, y, action;

        if (JH.state === JH.CONSTANTS.STATE_COUNTDOWN)
        {
            if (this.countdown === 3)
            {
                context.drawImage(this.threeImage, .5 * (me.video.getWidth() - this.threeImage.width), .5 * (me.video.getHeight() - this.threeImage.height));
            } else if (this.countdown === 2)
            {
                context.drawImage(this.twoImage, .5 * (me.video.getWidth() - this.twoImage.width), .5 * (me.video.getHeight() - this.twoImage.height));
            } else if (this.countdown === 1)
            {
                context.drawImage(this.oneImage, .5 * (me.video.getWidth() - this.oneImage.width), .5 * (me.video.getHeight() - this.oneImage.height));
            }
            else if (this.countdown === 0)
            {
                context.drawImage(this.driveImage, .5 * (me.video.getWidth() - this.driveImage.width), .5 * (me.video.getHeight() - this.driveImage.height));
            }

        } else if (JH.state === JH.CONSTANTS.STATE_PLAYING)
        {
            //UI style, adjust for the camera position to put in to screen coordinates
            //frame
            context.drawImage(this.frameImage, JH.CONSTANTS.FRAME_X + me.game.viewport.pos.x, JH.CONSTANTS.FRAME_Y + me.game.viewport.pos.y);

            if (JH.actionIndex < JH.actionList.length)
            {
                //arrows
                //for each step in the current action
                action = JH.actionList[JH.actionIndex];
                for (i = 0; i < action.stepList.length; i++)
                {
                    //pick the right arrow for the given step
                    if (action.stepList[i] === 'a') image = "up";
                    else if (action.stepList[i] === 'b') image = "down";
                    else if (action.stepList[i] === 'c') image = "right";

                    //if we've passed it, show the 'completed' version
                    if (action.stepIndex > i) image += "Complete";

                    image += "Image";

                    //draw the arrow
                    y = JH.CONSTANTS.ARROW_Y_START - (JH.CONSTANTS.ARROW_Y_MARGIN * (action.stepList.length - i - 1));
                    context.drawImage(this.arrows[image], JH.CONSTANTS.ARROW_X + me.game.viewport.pos.x, y + me.game.viewport.pos.y, 64, 64);
                }

                //next arrows
                if (JH.actionIndex < JH.actionList.length - 1)
                {
                    //for each step in the current action
                    action = JH.actionList[JH.actionIndex + 1];
                    for (i = 0; i < action.stepList.length; i++)
                    {
                        //pick the right arrow for the given step
                        if (action.stepList[i] === 'a') image = "up";
                        else if (action.stepList[i] === 'b') image = "down";
                        else if (action.stepList[i] === 'c') image = "right";

                        //if we've passed it, show the 'completed' version
                        if (action.stepIndex > i) image += "Complete";

                        image += "Image";

                        //draw the arrow
                        y = JH.CONSTANTS.NEXT_ARROW_Y_START - (JH.CONSTANTS.NEXT_ARROW_Y_MARGIN * (action.stepList.length - i - 1));
                        context.drawImage(this.arrows[image], JH.CONSTANTS.NEXT_ARROW_X + me.game.viewport.pos.x, y + me.game.viewport.pos.y, 32, 32);
                    }
                }
            }

            //hammer
            context.drawImage(this.hammerImage, JH.CONSTANTS.HAMMER_X + me.game.viewport.pos.x, JH.CONSTANTS.HAMMER_Y_TOP + (this.beatProgress * JH.CONSTANTS.HAMMER_Y_BOTTOM) + me.game.viewport.pos.y);
        }
        // else if state === level_end, don't draw any UI
    }
});

///////////////////////////////////////////////////////////
//  PARTICLE ENTITY
///////////////////////////////////////////////////////////
JH.ParticleEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        var w, h, image;
        w = settings.spritewidth;
        h = settings.spriteheight;
        image = me.loader.getImage(settings.image);
        settings.spritewidth = image.width; //correct for when particles are bigger than  image
        settings.spriteheight = image.height;
        this.parent(x, y, settings);
        this.r = settings.r;
        this.g = settings.g;
        this.b = settings.b;
        this.a = 1;
        this.width = w;
        this.height = h;
        this.tweener = new me.Tween(this);
        this.time = 0;
        this.tweener.to({ time: 1 }, settings.duration);
        this.vel.x = settings.velX;
        this.vel.y = settings.velY;
        this.accel = new me.Vector2d(settings.accelX, settings.accelY);
        this.gravity = settings.gravity || 0;
        this.collidable = false;
        this.float = settings.float !== false;

        this.lastViewport = me.game.viewport.pos.x;

        this.tweener.onComplete(this.destroy);
        this.tweener.start();
    },

    update: function () {
        //this.time should be updated by the tweener
        this.vel.x += this.accel.x;
        this.vel.y += this.accel.y;
        //this.renderable.setOpacity(1 - (.5 * this.time));
        //this.renderable.resize(1.5 - this.time);

        this.a = 1 - (.5 * this.time);

        this.updateMovement();

        if (this.float &&
            me.game.viewport.pos.x !== this.lastViewport)
        {
            this.pos.x += me.game.viewport.pos.x - this.lastViewport;
            this.lastViewport = me.game.viewport.pos.x;
        }

        this.parent(this);
        return true;
    },

    draw: function (context) {
        //context.globalOpacity = this.a;
        context.fillStyle = "rgba(" + this.r + ", " + this.g + ", " + this.b + ", " + this.a + ")";
        context.fillRect(this.pos.x, this.pos.y, this.width, this.height);
        //context.fillRect(this.pos.x + me.game.viewport.pos.x, this.pos.y + me.game.viewport.pos.y, this.width, this.height);
        //context.globalOpacity = 1;
    },

    destroy: function () {
        this.alive = false;
        me.game.remove(this);
    },
});

JH.spawnParticles = (function (amount, dir, spread, x, y, settings) {

    var unit, i, angle, part, size, vel, baseSize, red, green, blue, dur, variance, baseVel, baseDur, grav, dirVec;
    baseVel = settings.vel || 3.2;
    baseSize = settings.size || 20;
    red = settings.r || 245;
    green = settings.g || 240;
    blue = settings.b || 86;
    variance = settings.variance || 1;
    baseDur = settings.duration || 500;
    grav = settings.gravity || .1;
    dirVec = new me.Vector2d(dir.x, dir.y);
    dirVec.normalize();

    unit = new me.Vector2d(0, 0);
    for (i = 0; i < amount; i++)
    {
        angle = unit.angle(dirVec);
        angle += (.5 * spread) - (spread * Math.random());
        size = 1 + (variance * Math.random());
        vel = baseVel * (2 * (variance * Math.random()));
        dur = baseDur * (1 + (variance * Math.random()));
        part = me.entityPool.newInstanceOf('particle', x, y, {
            image: 'particle', spritewidth: baseSize * (size), spriteheight: baseSize * (size),
            velX: vel * Math.cos(angle), velY: vel * Math.sin(angle),
            accelX: 0, accelY: 0,
            duration: dur,
            r: red, g: green, b: blue,
            gravity: grav,
            float: settings.float
        });
        me.game.add(part, settings.layer);
    }
    me.game.sort();
});

///////////////////////////////////////////////////////////
//  MAP ACTION ENTITY
//  -- This translates the action objects from the tiled map to ActionObjects, which just holds the needed data
//  -- All of this happens in init.
///////////////////////////////////////////////////////////
JH.MapActionEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        var action;
        this.parent(x, y, settings);

        action = {};
        action.x = x;
        action.y = y;
        action.stepList = settings.steps.split(''); //makes an array of characters
        action.stepIndex = 0;
        //explicitly say this is not in a sequence
        action.isSequenceStart = false;
        action.isSequenceEnd = false;
        action.sequenceIndex = undefined;

        //push the action object to the global list
        JH.actionList.push(action);

        me.game.remove(this); //done after init

        //for (i = 0; i < settings.steps.length; i++)
        //{
        //    x = this.pos.x + 64 * i;
        //    //get a new step
        //    step = me.entityPool.newInstanceOf('step', x, y, { value: settings.steps[i] });
        //    //add step to beatmanager
        //    JH.actionList.push(step);
        //    //add step to game
        //    me.game.add(step, 4);
        //}
        //me.game.sort();
    },

    update: function ()
    {
        return false;
    },

});

///////////////////////////////////////////////////////////
//  MAP SEQUENCE ENTITY
//  -- This translates the sequence objects from the tiled map to a list of ActionObjects, which just holds the needed data
//     This will include some index values to hold the sequence data.
//  -- All of this happens in init.
///////////////////////////////////////////////////////////
JH.MapSequenceEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        var action, i, stepCount, currentStep;
        stepCount = settings.stepCount;
        this.parent(x, y, settings);

        for (i = 0; i < stepCount; i++)
        {
            //get the name of the current step
            currentStep = 'step' + i;

            action = {};
            action.x = x - 32;
            action.y = y - 48;
            action.stepList = settings[currentStep].split(''); //makes an array of characters
            action.stepIndex = 0;
            //explicitly say this IS in a sequence
            action.isSequenceStart = (i === 0); // true iff i is 0
            action.isSequenceEnd = (i === (stepCount-1)); //true iff i is stepCount-1
            action.sequenceIndex = i; //let the global container know where this is in the sequence

            //push the action object to the global list
            JH.actionList.push(action);
        }
    }
});

///////////////////////////////////////////////////////////
//  TRAIN ENTITY
//  -- This will be spawned by the player. It just moves forward for the duration of the music
//     If it gets to its destination, the player loses
///////////////////////////////////////////////////////////
JH.TrainEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        settings.image = "train";
        settings.name = settings.image;
        settings.spritewidth = 256;
        settings.spriteheight = 196;
        this.parent(x, y, settings);

        this.begin = x;
        this.end = me.game.currentLevel.cols * me.game.currentLevel.tilewidth - this.collisionBox.width * 1.5;

        this.startTime = 0;
        this.endTime = 0;
        this.particleCooldown = 0;
        this.move = false;
        //this.tweener = new me.Tween(this);
        //this.tweener.to({ t: 1 }, JH.CONSTANTS.SONG_LENGTH_SECONDS * 1000);
        //this.tweener.easing(me.Tween.Easing.Quintic.InOut);
        //this.tweener.onComplete(function () {
        //    // end the game -- failure
        //});
        //this.tweener.start();
        
    },

    start: function () {
        this.startTime = JH.currTime;
        this.endTime = this.startTime + JH.CONSTANTS.SONG_LENGTH_MILLIS;
        this.move = true;
    },

    update: function () {
        if (!this.move) return;

        //update position based on time
        this.pos.x = this.begin + ((JH.currTime - this.startTime) / JH.CONSTANTS.SONG_LENGTH_MILLIS) * (this.end - this.begin);
        this.particleCooldown -= JH.millis;

        //if cooldown is done, spawn some particles
        if (this.particleCooldown <= 0)
        {
            //spawn particles
            JH.spawnParticles(8, { x: 0, y: -1 }, .3 * Math.PI,
                this.pos.x + .6 * this.collisionBox.width, this.pos.y + 10,
                { layer: JH.CONSTANTS.PARTICLE_LAYER, r: 15, g: 15, b: 30, gravity: -.05, float: false, duration: JH.CONSTANTS.TRAIN_PARTICLE_COOLDOWN * 1.2, vel: 1 });
            this.particleCooldown += JH.CONSTANTS.TRAIN_PARTICLE_COOLDOWN;
        }

        if (JH.currTime >= this.endTime)
        {
            //end the game - failure
            me.state.change(me.state.GAMEOVER);
            me.audio.stopTrack();
        }
    }
});


///////////////////////////////////////////////////////////
//  STEP ENTITY
//  -- This is a single input cue for the user. It will be added programmatically by the sequence entity in order for the beatkeeper to keep track of it
//  -- only needs to be initialized, the beat keeper will remove it when necessary.
///////////////////////////////////////////////////////////
JH.StepEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings)
    {
        var h;
        if (settings.value === 'a')
        {
            settings.image = 'up';
            h = y - 16;
        } else if (settings.value === 'b')
        {
            settings.image = 'down';
            h = y + 16;
        } else //value === 'c'
        {
            settings.image = 'right';
            h = y;
        }

        settings.spritewidth = 32;
        settings.spriteheight = 32;
        this.parent(x, h, settings);
        this.value = settings.value;
    },

    update: function ()
    {
        this.parent(this);
        return true;
    }

});

///////////////////////////////////////////////////////////
//  TRACK ENTITY
//  -- This lets the track get built behind the player and the train. The player will create a row of these
//  -- which become visible when the player / train passes them
///////////////////////////////////////////////////////////
JH.TrackEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings) {
        settings.image = "track";
        settings.spriteheight = 32;
        settings.spritewidth = 32;
        this.parent(x, y, settings);
        this.followPlayer = settings.followPlayer !== false;
    },

    draw: function (context) {
        if (this.followPlayer === true) {
            if (JH.player.pos.x + 64 > this.pos.x ) {
                this.parent(context);
            }
        } else {
            if (JH.player.train.pos.x + 128 > this.pos.x ) {
                this.parent(context);
            }
        }
    }
});

///////////////////////////////////////////////////////////
//  OBSTACLE ENTITY
//  -- Visual indicator for an obstacle. It will be removed once the player passes it.
///////////////////////////////////////////////////////////
JH.ObstacleEntity = me.ObjectEntity.extend(
{
    init: function (x, y, settings) {
        settings.image = settings.image || 'rock';
        settings.spriteheight = 128;
        settings.spritewidth = 128;
        this.parent(x, y, settings);
    },

    draw: function (context) {
        if (JH.player.pos.x < this.pos.x + 8) {
            this.parent(context);
        }
    }
});