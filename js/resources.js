game.resources = [

	/* Graphics. 
	 * @example
	 * {name: "example", type:"image", src: "data/img/example.png"},
	 */

     //main entities
	{ name: "john", type: "image", src: "res/img/john.png" },
	{ name: "jh", type: "image", src: "res/img/jh.png" },
	{ name: "particle", type: "image", src: "res/img/jh.png" },
	{ name: "train", type: "image", src: "res/img/train.png" },
	{ name: "track", type: "image", src: "res/img/track.png" },
	{ name: "rock", type: "image", src: "res/img/rock.png" },
	{ name: "tree", type: "image", src: "res/img/tree.png" },

    //level art
	{ name: "bg", type: "image", src: "res/img/bg.png" },
	{ name: "tilemap", type: "image", src: "res/img/tilemap.png" },
	{ name: "metatiles32x32", type: "image", src: "res/img/metatiles32x32.png" },
	{ name: "level1_sky", type: "image", src: "res/img/level1_sky.png" },
	{ name: "level1_middle", type: "image", src: "res/img/level1_middle.png" },
	{ name: "level1_front", type: "image", src: "res/img/level1_front.png" },
	{ name: "level2_skyt", type: "image", src: "res/img/level2_skyt.png" },
	{ name: "level2_middle", type: "image", src: "res/img/level2_middle.png" },
	{ name: "level2_front", type: "image", src: "res/img/level2_front.png" },
	{ name: "level3_sky", type: "image", src: "res/img/level3_sky.png" },
	{ name: "level3_middle", type: "image", src: "res/img/level3_middle.png" },
	{ name: "level3_front", type: "image", src: "res/img/level3_front.png" },
	{ name: "level4_sky", type: "image", src: "res/img/level4_sky.png" },
	{ name: "level4_middle", type: "image", src: "res/img/level4_middle.png" },
	{ name: "level4_middle2", type: "image", src: "res/img/level4_middle2.png" },
	{ name: "level4_front", type: "image", src: "res/img/level4_front.png" },
	{ name: "level5_sky", type: "image", src: "res/img/level5_sky.png" },
	{ name: "level5_middle", type: "image", src: "res/img/level5_middle.png" },
	{ name: "level5_middle2", type: "image", src: "res/img/level5_middle2.png" },
	{ name: "level5_front", type: "image", src: "res/img/level5_front.png" },

    //ui elements
    { name: "up", type: "image", src: "res/img/up.png" },
	{ name: "down", type: "image", src: "res/img/down.png" },
	{ name: "right", type: "image", src: "res/img/right.png" },
    { name: "up_complete", type: "image", src: "res/img/up_complete.png" },
	{ name: "down_complete", type: "image", src: "res/img/down_complete.png" },
	{ name: "right_complete", type: "image", src: "res/img/right_complete.png" },
	{ name: "3", type: "image", src: "res/img/3.png" },
	{ name: "2", type: "image", src: "res/img/2.png" },
	{ name: "1", type: "image", src: "res/img/1.png" },
	{ name: "drive", type: "image", src: "res/img/drive.png" },
	{ name: "hammer", type: "image", src: "res/img/hammer.png" },
	{ name: "frame", type: "image", src: "res/img/frame.png" },
	{ name: "win", type: "image", src: "res/img/win.png" },
	{ name: "lose", type: "image", src: "res/img/lose.png" },

    //menu screens
	{ name: "credits_bg", type: "image", src: "res/img/credits_bg.png" },
	{ name: "game_end_bg", type: "image", src: "res/img/game_end_bg.png" },
	{ name: "game_over_bg", type: "image", src: "res/img/game_over_bg.png" },
	{ name: "title_bg", type: "image", src: "res/img/title_bg.png" },
	{ name: "selection", type: "image", src: "res/img/selection.png" },
	{ name: "play", type: "image", src: "res/img/play.png" },
	{ name: "credits", type: "image", src: "res/img/credits.png" },


	/* Maps. 
	 * @example
	 * {name: "example01", type: "tmx", src: "data/map/example01.tmx"},
	 * {name: "example01", type: "tmx", src: "data/map/example01.json"},
 	 */
	{ name: "level_1", type: "tmx", src: "res/level_1.tmx" },
	{ name: "level_2", type: "tmx", src: "res/level_2.tmx" },
	{ name: "level_3", type: "tmx", src: "res/level_3.tmx" },
	{ name: "level_4", type: "tmx", src: "res/level_4.tmx" },
	{ name: "level_5", type: "tmx", src: "res/level_5.tmx" },

	/* Background music. 
	 * @example
	 * {name: "example_bgm", type: "audio", src: "data/bgm/", channel : 1},
	 */
    { name: "music", type: "audio", src: "res/sfx/", channel: 1 },

	
	/* Sound effects. 
	 * @example
	 * {name: "example_sfx", type: "audio", src: "data/sfx/", channel : 2}
	 */
	{ name: "a", type: "audio", src: "res/sfx/", channel: 2 },
	{ name: "b", type: "audio", src: "res/sfx/", channel: 3 },
	{ name: "c", type: "audio", src: "res/sfx/", channel: 4 },
	{ name: "drum", type: "audio", src: "res/sfx/", channel: 1 },
	{ name: "hit", type: "audio", src: "res/sfx/", channel: 5 },
	{ name: "miss", type: "audio", src: "res/sfx/", channel: 5 },
	{ name: "success", type: "audio", src: "res/sfx/", channel: 5 },
];
