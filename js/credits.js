game.CreditsScreen = me.ScreenObject.extend(
{
	init: function()
	{ 
		this.parent(true, true);
		this.background = null; 
	}, 
	
	/**	
	 *  action to perform on state change
	 */
	onResetEvent: function() {	
			
            this.background = me.loader.getImage("credits_bg");;

	},
	
	update: function()
	{
		this.parent();
        if (me.input.isKeyPressed("enter"))
			me.state.change(me.state.TITLE);
		
		return true;
	},
	
	draw: function(context)
	{
        context.drawImage(this.background, 0,0, me.video.getWidth(), me.video.getHeight());
	}
});

game.GameEndScreen = me.ScreenObject.extend(
{
    ready: false,

    init: function () {
        this.parent(true, true);
        this.background = null;
        this.win = null;
    },

    /**	
	 *  action to perform on state change
	 */
    onResetEvent: function () {
        if (me.game.viewport._fadeIn.tween) me.game.viewport._fadeIn.tween.stop();
        if (me.game.viewport._fadeIn.alpha) me.game.viewport._fadeIn.alpha = 0;

        this.background = me.loader.getImage("game_end_bg");
        this.win = me.loader.getImage("win");
        game.GameEndScreen.ready = false;
        window.setTimeout(function () { game.GameEndScreen.ready = true; }, 3000);

    },

    update: function () {
        this.parent();
        if (me.input.isKeyPressed("enter"))
            me.state.change(me.state.TITLE);

        return true;
    },

    draw: function (context) {
        context.drawImage(this.background, 0, 0, me.video.getWidth(), me.video.getHeight());
        if (game.GameEndScreen.ready === true) {
            context.drawImage(this.win, .5 * (me.video.getWidth() - this.win.width),
                .5 * (me.video.getHeight() - this.win.height),
                this.win.width, this.win.height);
        }
    }
});

game.GameOverScreen = me.ScreenObject.extend(
{
    ready: false,

    init: function () {
        this.parent(true, true);
        this.background = null;
        this.win = null;
        this.ready = false;

    },

    /**	
	 *  action to perform on state change
	 */
    onResetEvent: function () {
        if (me.game.viewport._fadeIn.tween) me.game.viewport._fadeIn.tween.stop();
        if (me.game.viewport._fadeIn.alpha) me.game.viewport._fadeIn.alpha = 0;

        this.background = me.loader.getImage("game_over_bg");
        this.lose = me.loader.getImage("lose");
        game.GameOverScreen.ready = false;
        window.setTimeout(function () { game.GameOverScreen.ready = true; }, 3000);

    },

    update: function () {
        this.parent();
        if (me.input.isKeyPressed("enter"))
            me.state.change(me.state.TITLE);

        return true;
    },

    draw: function (context) {
        context.drawImage(this.background, 0, 0, me.video.getWidth(), me.video.getHeight());
        if (game.GameOverScreen.ready === true) {
            context.drawImage(this.lose, .5 * (me.video.getWidth() - this.lose.width),
                .5 * (me.video.getHeight() - this.lose.height),
                this.lose.width, this.lose.height);
        }
    }
});
