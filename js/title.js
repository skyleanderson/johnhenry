/*
* menu screen
*/

//namespace declaration
var JH = JH || {};
JH.GLOBALS = JH.GLOBALS || {};

game.TitleScreen = me.ScreenObject.extend(
{
        /*
        * constructor
        */
        init: function()
        {
            // call parent constructor
            this.parent(true, true);
            
            // init stuff
            this.background = null;
            this.play = null;
            this.credits = null;
            this.version = null;
            this.selection = null;

            JH.GLOBALS.muted = 0;
			
            JH.GLOBALS.title_index = 0;
        },
        
        
        /*
        * reset function
        */
        onResetEvent: function()
        {
            
            // load title image
        	this.background = me.loader.getImage("title_bg");
            this.selection = me.loader.getImage("selection");
            
            // play button
            this.play    = new Button(0, "play", me.state.PLAY, 610, 224);
            this.credits = new Button(1, "credits", me.state.CREDITS, 610, 386);
           
//            this.mute = new MuteButton(me.video.getWidth() - 35, me.video.getHeight() - 35); 
            
            // version
            this.version = new me.Font("Verdana", 18, "#dddddd");;
            me.game.sort();
        },
        
        /**
         * update function
         */
        update: function()
        {
             this.parent();
             
             if (me.input.isKeyPressed("bot"))
 				JH.GLOBALS.title_index = 1;
             else if (me.input.isKeyPressed("top"))
  				JH.GLOBALS.title_index = 0;
        	 

			//check if the mouse is hovered
			if (this.play.containsPoint(me.input.mouse.pos))
			{
				JH.GLOBALS.title_index = 0;
			} else if (this.play.containsPoint(me.input.mouse.pos))
			{
				JH.GLOBALS.title_index = 1;
			}

             if (me.input.isKeyPressed("enter")) 
             {
                 if (JH.GLOBALS.title_index === 0)
            		 me.state.change(me.state.PLAY);
            	 else
            		 me.state.change(me.state.CREDITS);
            		 
             }
             return true;
        },
                 
        /*
        * drawing function
        */
        draw: function(context)
        {
            // draw title
            context.drawImage(this.background, 0,0, me.video.getWidth(), me.video.getHeight());
                
            // draw play button
            this.play.draw(context);
            this.credits.draw(context);
            
            //draw selection indicator
            context.drawImage(this.selection, 570, this.play.pos.y - 12 + (162 * JH.GLOBALS.title_index));
                
//            this.mute.draw(context);
			var versionText = "0.1";
			var versionSize = this.version.measureText(context, versionText);
			this.version.draw(context, versionText,
				me.video.getWidth() - versionSize.width - 5, me.video.getHeight() - versionSize.height - 5);
        },
        
        /*
        * destroy event function
        */
        onDestroyEvent: function()
        {
            // release mouse event
            me.input.releasePointerEvent("mousedown", this.play);
        }
});

/*
* draw a button on screen. Borrowed from invasionJS by semche
*/
var Button = me.Rect.extend(
{
        /*
        * constructor
        */
        init: function(index, image, action, x, y)
        {
            // init stuff
        	this.index = index;
            this.image = me.loader.getImage(image);
            this.action = action;
            
            //if x or y is -1, center on screen
            var endx = x >= 0 ? x : (me.video.getWidth()  / 2 - this.image.width / 2);
            var endy = y >= 0 ? y : (me.video.getHeight() / 2 - this.image.height / 2);
            
            this.pos = new me.Vector2d(endx, endy);
            
            // call parent constructor
            this.parent(this.pos, this.image.width, this.image.height);
            
            // register mouse event
            me.input.registerPointerEvent("mousedown", this, this.clicked.bind(this));
        },
        
        /*
        * action to perform when a button is clicked
        */
        clicked: function()
        {
            // start action
            me.state.change(this.action);
        },
        
        /*
        * drawing function
        */
        draw: function(context)
        {
            //context.drawImage(this.image, this.pos.x, this.pos.y);
        },
        
        update: function()
        {

            this.parent();
            return true;
        },
        
        /*
        * destroy event function
        */
        onDestroyEvent: function()
        {
            // release mouse events
            me.input.releasePointerEvent("mousedown", this);
        }
});

/*
* draw a button on screen. Borrowed from invasionJS by semche
*/
var MuteButton = me.Rect.extend(
{
        /*
        * constructor
        */
        init: function(x, y)
        {
                // init stuff
                this.muteImage        = me.loader.getImage("mute");
                this.muteImageHover   = me.loader.getImage("mute_hover");
                this.unmuteImage      = me.loader.getImage("unmute");
                this.unmuteImageHover = me.loader.getImage("unmute_hover");
                this.muted = JH.GLOBALS.muted;
                
                this.pos = new me.Vector2d(x, y);
                
                // call parent constructor
                this.parent(this.pos, this.muteImage.width, this.muteImage.height);
                
                // register mouse event
                me.input.registerPointerEvent("mousedown", this, this.clicked.bind(this));
        },
        
        /*
        * action to perform when a button is clicked
        */
        clicked: function()
        {
                // toggle states
                this.muted = !this.muted;
                JH.GLOBALS.muted = this.muted;

                if (this.muted)
                        me.audio.muteAll();
                else
                        me.audio.unmuteAll();
        },
        
        /*
        * drawing function
        */
        draw: function(context)
        {
                // on button hovered
                if (this.muted)
                {
                        if (this.containsPoint(me.input.mouse.pos))
                                context.drawImage(this.muteImageHover, this.pos.x, this.pos.y);
                        else
                                context.drawImage(this.muteImage, this.pos.x, this.pos.y);
                } else //not muted
                {
                        if (this.containsPoint(me.input.mouse.pos))
                                context.drawImage(this.unmuteImageHover, this.pos.x, this.pos.y);
                        else
                                context.drawImage(this.unmuteImage, this.pos.x, this.pos.y);
                }
        },
        
        update: function()
        {
                this.parent();
                return true;
        },
        
        /*
        * destroy event function
        */
        onDestroyEvent: function()
        {
                // release mouse events
                me.input.releasePointerEvent("mousedown", this);
        }
});