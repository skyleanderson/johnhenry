///* Game namespace */
var game = {
    // Run on page load.
    "onload" : function () {
        // Initialize the video.
        if (!me.video.init("screen", 960, 640, true, 'auto')) {
            alert("Your browser does not support HTML5 canvas.");
            return;
        }
		
		// add "#debug" to the URL to enable the debug Panel
		if (document.location.hash === "#debug") {
			window.onReady(function () {
				me.plugin.register.defer(debugPanel, "debug");
			});
		}

        // Initialize the audio.
        me.audio.init("mp3,ogg");

        // Set a callback to run when loading is complete.
        me.loader.onload = this.loaded.bind(this);
     
        // Load the resources.
        me.loader.preload(game.resources);

        // Initialize melonJS and display a loading screen.
        me.state.change(me.state.LOADING);
    },



    // Run on game resources loaded.
    "loaded" : function () {
        //me.debug.renderHitBox = true;

    	//entities
        me.entityPool.add("player", JH.PlayerEntity);
        me.entityPool.add("particle", JH.ParticleEntity);
        me.entityPool.add("action", JH.MapActionEntity);
        me.entityPool.add("sequence", JH.MapSequenceEntity);
        me.entityPool.add("train", JH.TrainEntity);
        me.entityPool.add("track", JH.TrackEntity);
        me.entityPool.add("obstacle", JH.ObstacleEntity);

        me.state.set(me.state.TITLE, new game.TitleScreen());
        me.state.set(me.state.PLAY, new game.PlayScreen());
        me.state.set(me.state.CREDITS, new game.CreditsScreen());
        me.state.set(me.state.GAME_END, new game.GameEndScreen());
        me.state.set(me.state.GAMEOVER, new game.GameOverScreen());
                
        //controls
		me.input.bindKey(me.input.KEY.UP,  "top", true);
		me.input.bindKey(me.input.KEY.RIGHT, "mid", true);
		me.input.bindKey(me.input.KEY.LEFT, "mid2", true);
		me.input.bindKey(me.input.KEY.DOWN, "bot", true);
		me.input.bindKey(me.input.KEY.ENTER,  "enter", true);

        // Start the game.
		me.state.transition("fade", "#ffffff", 250);
        me.state.change(me.state.TITLE);
    }
};
