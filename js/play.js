//namespace declaration
var JH = JH || {};

game.PlayScreen = me.ScreenObject.extend(
{
    /**     
     *  action to perform on state change
     */
    onResetEvent: function() 
    {      
        if (!JH.GLOBALS.MAX_LEVEL) JH.GLOBALS.MAX_LEVEL = 5;
        
//        me.gamestat.setValue("health", 5);
        JH.level = 1;
        me.levelDirector.loadLevel("level_" + JH.level);
        
//        me.game.addHUD(5,5, me.video.getWidth()-10, 20);
//        me.game.HUD.addItem("score", new game.ScoreObject(0, 0));
	},


	/**     
	 *  action to perform when leaving this screen (state change)
	 */
	onDestroyEvent: function() 
	{
	    //me.game.disableHUD();
	    me.game.removeAll();
	}
});
